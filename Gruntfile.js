/*
 * grunt-html-factory-grunticon-finisher
 * 
 *
 * Copyright (c) 2017 Ondrej Bohac
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {


  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>'
      ],
        options: {
            jshintrc: '.jshintrc',
        }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    html_factory_grunticon_finisher: {
        options: {
            pathToPngFile: "icons.data.png.css",
            pathToSvgFile: "icons.data.svg.css",
            targetPngFile: "icons.data.png.css",
            targetSvgFile:  "icons.data.svg.css",
            pathToDimensionsFile:  "icons.data.dimensions.css"
        },
        html_factory_grunticon_finisher: {
        }
    },

    // Unit tests.
    nodeunit: {
      tests: ['test/*_test.js']
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');

  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['clean', 'html_factory_grunticon_finisher', 'nodeunit']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'test']);

};
