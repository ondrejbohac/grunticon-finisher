/*
 * grunt-html-factory-grunticon-finisher
 * 
 *
 * Copyright (c) 2017 Ondrej Bohac
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {
    grunt.registerMultiTask('html_factory_grunticon_finisher', 'Grun html factory grunticon finisher', function () {
        grunt.log.writeln("task loaded");
        var pathToPngFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["pathToPngFile"];
        var pathToSvgFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["pathToSvgFile"];
        var pathToFallbackFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["pathToFallbackFile"];
        var targetToPngFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["targetPngFile"];
        var targetToSvgFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["targetSvgFile"];
        var targetToFallbackFile = grunt.config.data["html_factory_grunticon_finisher"]["options"]["targetFallbackFile"];
        var targetToDimensionsFile =grunt.config.data["html_factory_grunticon_finisher"]["options"]["targetDimensionsFile"];

        console.log(pathToPngFile);

        var cssProperties = {};

        var css = require('css');
        // var filePng = grunt.file.read('assets\\images\\sprites\\grunticon\\icons.data.png.css');
        var filePng = grunt.file.read(pathToPngFile);
        var fileSvg = grunt.file.read(pathToSvgFile);
        var fileFallback = grunt.file.read(pathToFallbackFile);

        var objPng = css.parse(filePng);
        var objSvg = css.parse(fileSvg);
        var objFallback = css.parse(fileFallback);

        var parser = function(otherPrefix,file) {
            var rules = file["stylesheet"]["rules"];
            for (var key in rules) {
                var selector = rules[key]["selectors"];
                var declarations = rules[key]["declarations"];

                if(typeof cssProperties[selector] == "undefined")
                {
                    cssProperties[selector] = {};
                }
                if(typeof cssProperties[selector][otherPrefix] == "undefined")
                {
                    cssProperties[selector][otherPrefix] = {};
                }

                for (var declarationKey in declarations) {
                    var declaration = declarations[declarationKey];

                    if(["width","height"].indexOf(declaration["property"]) != -1){
                        cssProperties[selector][declaration["property"]] = declaration["value"];
                    }else{
                        cssProperties[selector][otherPrefix][declaration["property"]] = declaration["value"];
                    }
                }
            }
        };

        parser("png",objPng);
        parser("svg",objSvg);
        parser("fallback",objFallback);

        var filePNG = "";
        var fileSVG = "";
        var fileFALLBACK = "";
        var fileDimensions = "";

        for (var key in cssProperties) {
            if (cssProperties.hasOwnProperty(key)) {
                var selector = cssProperties[key];

                filePNG += (key + " { \n");
                fileSVG += (key + " { \n");
                fileFALLBACK += (key + " { \n");
                fileDimensions += (key + " { \n");

                var writer = function(key, object){
                    var properties = "";
                    for (var propertyKey in object[key]) {
                        var property = object[key][propertyKey];
                        properties += "    "+propertyKey+": " + property +";\n";
                    }
                    return properties;
                };

                filePNG += writer("png", selector);
                fileSVG += writer("svg", selector);
                fileFALLBACK += writer("fallback", selector);

                fileDimensions += "    width: " + selector["width"]+";\n";
                fileDimensions += "    height: " + selector["height"]+";\n";

                filePNG += "} \n\n";
                fileSVG += "} \n\n";
                fileFALLBACK += "} \n\n";
                fileDimensions += "} \n\n";
            }
        }

        grunt.file.write(targetToPngFile, filePNG);
        grunt.file.write(targetToSvgFile, fileSVG);
        grunt.file.write(targetToFallbackFile, fileFALLBACK);
        grunt.file.write(targetToDimensionsFile, fileDimensions);
    });
};
